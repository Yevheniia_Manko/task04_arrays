package com.manko.task04.part02game;

public class Hero {

    private int health = 25;

    public Hero() {

    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int power) {
        health += power;
    }

    @Override
    public String toString() {
        return ("Your health is " + health + " now.");
    }
}
