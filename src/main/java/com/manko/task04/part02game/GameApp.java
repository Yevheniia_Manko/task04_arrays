package com.manko.task04.part02game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GameApp {

    private static Logger logger = LogManager.getLogger(GameApp.class);

    public static void main(String[] args) {
        Game game = new Game();
        game.createDoors();
        int danger = game.calculateDangerousDoors(game.getNumberOfDoors());
        logger.info("You will be killed if you open " + danger + " doors from " + game.getNumberOfDoors());
        game.findWinningCombination();
        game.isChance();
        game.startGame();
        game.printWinningCombination();
        logger.info("The things behind the doors at the start of the game:");
        game.printInfo();
    }
}
