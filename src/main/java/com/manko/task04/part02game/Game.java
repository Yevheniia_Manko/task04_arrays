package com.manko.task04.part02game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * This is not a finished version of the game. It needs to be updated.
 * @author Yevheniia Manko
 * @version 1.0
 */


public class Game {

    private Hero hero;
    private int numberOfDoors = 10;
    private int numberOfDangerousDoors;
    private int[] doors;
    private int[] openedDoors;
    private int[] winningDoors = new int[numberOfDoors];
    private Scanner sc;
    private static Logger logger = LogManager.getLogger(Game.class);

    public Game() {
        sc = new Scanner(System.in);
        hero = new Hero();
        doors = new int[numberOfDoors];
        openedDoors = new int[numberOfDoors];
    }

    public int getNumberOfDoors() {
        return numberOfDoors;
    }

    public int calculateDangerousDoors (int doorsCount) {
        if (doorsCount == 0) {
            return numberOfDangerousDoors;
        }
        if (hero.getHealth() < (- doors[--doorsCount])) {
            numberOfDangerousDoors++;
        }
        return calculateDangerousDoors(doorsCount);
    }

    public void createDoors() {
        for (int i = 0; i < numberOfDoors; i++) {
            boolean artifact = new Random().nextBoolean();
            if (artifact) {
                int artifactPower = (int) (Math.random() * 71 + 10);
                doors[i] = artifactPower;
            } else {
                int monsterPower = (int) (Math.random() * 96 + 5);
                doors[i] = - monsterPower;
            }
        }
    }

    public void printInfo() {
        logger.info("Door\t" + "Artifact/Monster\t" + "Power");
        for (int i = 0; i < doors.length; i++) {
            if (doors[i] < 0) {
                logger.info((i + 1) + "\t\t\t" + "monster\t\t\t" + (-doors[i]));
            } else {
                logger.info((i + 1) + "\t\t\t" + "artifact\t\t" + doors[i]);
            }
        }
    }

    public void startGame() {
        int door;
        int count = 0;
        System.arraycopy(doors, 0, openedDoors, 0, numberOfDoors);
        while (count < numberOfDoors) {
            logger.info("You can open the next doors: ");
            for (int i = 0; i < numberOfDoors; i++) {
                if (openedDoors[i] != 0) {
                    System.out.print((i + 1) + " ");
                }
            }
            logger.info("\nInput the number of door:");
            door = readIntNumber();
            if (door == 0 || openedDoors[door - 1] == 0) {
                logger.info("Invalid number of the door! Try again.");
                continue;
            }
            makeTurn(door);
            openedDoors[door - 1] = 0;
            if (hero.getHealth() < 0) {
                logger.info("The monster won!");
                break;
            } else {
                if (numberOfDangerousDoors == 0) {
                    break;
                }
                count++;
            }
        }
        if (hero.getHealth() >= 0) {
            logger.info("You won!!!");
        }
    }

    private void makeTurn(int number) {
        if (doors[number - 1] < 0) {
            logger.info("You met the monster!");
            numberOfDangerousDoors--;
        } else {
            logger.info("You found the artifact!");
        }
        hero.setHealth(doors[number - 1]);
        logger.info(hero);
    }

    private int readIntNumber() {
        int userInput = 0;
        try{
            userInput = Integer.parseInt(sc.nextLine());
        } catch (Exception e) {
            logger.info("You entered incorrect data!");
        }
        return userInput;
    }

    public boolean isChance() {
        int totalMonsterPower = 0;
        int totalArtifactPower = 0;
        for (int i = 0; i < doors.length; i++) {
            if (doors[i] < 0) {
                totalMonsterPower += doors[i];
            } else {
                totalArtifactPower += doors[i];
            }
        }
        if (totalArtifactPower + hero.getHealth() < (- totalMonsterPower)) {
            logger.info("You have no chance!");
            return false;
        } else {
            logger.info("You can win the monster!");
            return true;
        }
    }

    public int[] findWinningCombination() {
        int k = 0;
        for (int i = 0; i < numberOfDoors; i++) {
            if (doors[i] > 0) {
                winningDoors[k] = i + 1;
                k++;
            }
        }
        for (int i = 0; i < numberOfDoors; i++) {
            if (doors[i] < 0) {
                winningDoors[k] = i + 1;
                k++;
            }
        }
        return winningDoors;
    }

    public void printWinningCombination() {
        logger.info("\nGAME INFORMATION:");
        boolean chance = isChance();
        logger.info("The winning combination of doors was:");
        if (chance) {
            Arrays.stream(winningDoors).forEach(s -> System.out.print(s + " "));
        } else {
            logger.info("no such combination");
        }
    }
}
