package com.manko.task04.part01.view;

import com.manko.task04.part01.controller.NumberArrayController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

import static java.util.Arrays.stream;

public class ConsoleUserMenu {

    private static final int MIN_MENU_NUMBER = 0;
    private static final int MAX_MENU_NUMBER = 4;
    private static Logger logger = LogManager.getLogger(ConsoleUserMenu.class);
    private NumberArrayController arrayController;
    private Scanner sc;
    private int[] array;

    public ConsoleUserMenu() {
        sc = new Scanner(System.in);
        arrayController = new NumberArrayController();
    }

    private void createArray() {
        logger.info("Please enter the size of the array:");
        array = fillArray();
        arrayController.createOneArray(array);
        logger.info("The numbers in the initial array are:");
        printArray(array);
    }

    private void createTwoArrays() {
        logger.info("Please enter the size of the first array:");
        array = fillArray();
        logger.info("Please enter the size of the second array:");
        int[] otherArray = fillArray();
        arrayController.createTwoArrays(array, otherArray);
        logger.info("The numbers in the first array are:");
        printArray(array);
        logger.info("The numbers in the second array are:");
        printArray(otherArray);
    }

    private int readIntegerValue() {
        int value;
        while (true) {
            try {
                value = Integer.parseInt(sc.nextLine());
                break;
            } catch (Exception e) {
                logger.info("You entered invalid value.");
                logger.info("Please try again.");
            }
        }
        return value;
    }

    private int[] fillArray() {
        int size = readIntegerValue();
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            logger.info("Please enter the number:");
            array[i] = readIntegerValue();
        }
        return array;
    }

    private int menu() {
        logger.info("Enter next number if you want ...");
        logger.info(" 1 - input two arrays and find out all common numbers in the arrays.");
        logger.info(" 2 - input two arrays and find out all unique numbers in the arrays.");
        logger.info(" 3 - input the array with repeating numbers and delete all repeats " +
                "that appear more than 3 times.");
        logger.info(" 4 - input the array that contains sequences of repeating numbers " +
                "and remove all repeating elements except the first.");
        logger.info(" 0 - exit.");
        return readMenuItem(MIN_MENU_NUMBER, MAX_MENU_NUMBER);
    }

    private int readMenuItem(int min, int max) {
        int choice;
        while (true) {
            choice = readIntegerValue();
            if (choice >= min && choice <= max) {
                break;
            } else {
                logger.info("You entered invalid value.");
                logger.info("Please enter the menu item.");
            }
        }
        return choice;
    }

    public void start() {
        logger.info("Welcome to the arrays learning room!");
        boolean oneMore = true;
        while (oneMore) {
            int choice = menu();
            switch (choice) {
                case 0:
                    logger.info("Have a nice day! Bye!");
                    oneMore = false;
                    break;
                case 1:
                    createTwoArrays();
                    arrayController.showCommonNumbers();
                    logger.info("The common numbers in two arrays are:");
                    printArray(arrayController.showArray());
                    break;
                case 2:
                    createTwoArrays();
                    arrayController.showUniqueNumbers();
                    logger.info("The unique numbers in two arrays are:");
                    printArray(arrayController.showArray());
                    break;
                case 3:
                    createArray();
                    arrayController.removeRepeats(3);
                    logger.info("All numbers that appear more than 3 times will be deleted.");
                    logger.info("Now the numbers in the array are:");
                    printArray(arrayController.showArray());
                    break;
                case 4:
                    createArray();
                    arrayController.removeSequence();
                    logger.info("Now the numbers in the array are:");
                    printArray(arrayController.showArray());
                    break;
                default:
                    break;
            }
        }
    }

    private void printArray(int[] numbers) {
        if (numbers.length == 0) {
            logger.info("not found");
        } else {
            stream(numbers).forEach(s -> System.out.print(s + " "));
        }
        logger.info("\n");
    }
}
