package com.manko.task04.part01.model;

public class NumberArray {

    private int[] firstArray;
    private int[] secondArray;
    private int[] resultArray;
    private int numberCount;

    public NumberArray(int[] array) {
        this.firstArray = array;
    }

    public NumberArray(int[] firstArray, int[] secondArray) {
        this.firstArray = firstArray;
        this.secondArray = secondArray;
    }

    public int[] getResultArray() {
        if (numberCount == 0) {
            resultArray = new int[0];
        }
        if (numberCount < resultArray.length) {
            resultArray = trimArray(resultArray, numberCount);
        }
        return resultArray;
    }

    public void selectCommonNumbers() {
        int[] array1 = deleteRepeatingNumbers(firstArray, 2);
        array1 = trimArray(array1, numberCount);
        firstArray = array1;
        int[] array2 = deleteRepeatingNumbers(secondArray, 2);
        array2 = trimArray(array2, numberCount);
        secondArray = array2;
        numberCount = 0;
        int resultArrayLength = (firstArray.length <= secondArray.length) ? firstArray.length : secondArray.length;
        resultArray = new int[resultArrayLength];
        for (int i = 0; i < firstArray.length; i++) {
            if (checkMatchingNumbers(secondArray, firstArray[i])) {
                resultArray[numberCount] = firstArray[i];
                numberCount++;
            }
        }
    }

    public void selectUniqueNumbers() {
        int[] array1 = deleteRepeatingNumbers(firstArray, 2);
        array1 = trimArray(array1, numberCount);
        firstArray = array1;
        int[] array2 = deleteRepeatingNumbers(secondArray, 2);
        array2 = trimArray(array2, numberCount);
        secondArray = array2;
        numberCount = 0;
        int resultArrayLength = firstArray.length + secondArray.length;
        resultArray = new int[resultArrayLength];
        for (int i = 0; i < firstArray.length; i++) {
            if (!checkMatchingNumbers(secondArray, firstArray[i])) {
                resultArray[numberCount] = firstArray[i];
                numberCount++;
            }
        }
        for (int i = 0; i < secondArray.length; i++) {
            if (!checkMatchingNumbers(firstArray, secondArray[i])) {
                resultArray[numberCount] = secondArray[i];
                numberCount++;
            }
        }
    }

    private boolean checkMatchingNumbers(int[] array, int number) {
        boolean foundNumber = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                foundNumber = true;
                break;
            }
        }
        return foundNumber;
    }

    private int[] trimArray(int[] numbers, int size) {
        int[] newResultArray = new int[size];
        System.arraycopy(numbers, 0, newResultArray, 0, size);
        return newResultArray;
    }

    private int[] deleteRepeatingNumbers(int[] array, int maxRepeats) {
        numberCount = 0;
        int[] repeatsCount = findRepeatingNumbers(array);
        int[] newArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            if (repeatsCount[i] < maxRepeats ) {
                newArray[numberCount] = array[i];
                numberCount++;
            }
        }
        return newArray;
    }

    public void deleteRepeatingNumbers(int maxRepeats) {
        resultArray = deleteRepeatingNumbers(firstArray, maxRepeats);
    }

    private int[] findRepeatingNumbers(int[] array) {
        int size = array.length;
        int[] repeats = new int[size];
        for (int i = 0; i < repeats.length; i++) {
            repeats[i] = 1;
        }
        for (int i = 0; i < size - 1; i++) {
            for (int j = i + 1; j < size; j++) {
                if (array[i] == array[j]) {
                    repeats[j]++;
                }
            }
        }
        return repeats;
    }

    public void deleteSequence() {
        resultArray = new int[firstArray.length];
        resultArray[0] = firstArray[0];
        numberCount = 1;
        for (int i = 1; i < firstArray.length; i++) {
            if (firstArray[i] == firstArray[i - 1]) {
                continue;
            }
            resultArray[numberCount] = firstArray[i];
            numberCount++;
        }
    }
}
