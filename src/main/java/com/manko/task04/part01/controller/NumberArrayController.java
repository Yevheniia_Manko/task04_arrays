package com.manko.task04.part01.controller;

import com.manko.task04.part01.model.NumberArray;

public class NumberArrayController {

    private NumberArray numberArray;

    public NumberArrayController() {

    }

    public void createOneArray(int[] array) {
        this.numberArray = new NumberArray(array);
    }

    public void removeRepeats(int maxRepeats) {
        numberArray.deleteRepeatingNumbers(maxRepeats);
    }

    public int[] showArray() {
        return numberArray.getResultArray();
    }

    public void createTwoArrays(int[] firstArray, int[] secondArray) {
        numberArray = new NumberArray(firstArray, secondArray);
    }

    public void showCommonNumbers() {
        numberArray.selectCommonNumbers();
    }

    public void showUniqueNumbers() {
        numberArray.selectUniqueNumbers();
    }

    public void removeSequence() {
        numberArray.deleteSequence();
    }
}
